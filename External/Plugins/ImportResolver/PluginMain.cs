﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using ASCompletion.Completion;
using ASCompletion.Context;
using ASCompletion.Model;
using PluginCore;
using PluginCore.Helpers;
using PluginCore.Managers;
using PluginCore.Utilities;
using ScintillaNet;

namespace ImportResolver
{
    public class PluginMain : IPlugin
    {
        //settings file and folder names
        private const string SettingsFile = "ImportResolver_v1.fdb";
        private const string SettingsFolder = "ImportResolver";

        // shortcut name
        internal const string Shortcut = "ImportResolver.AddMissingImports";
        private const string PluginAuth = "m0rkeulv";
        private const string PluginDesc = "";
        private const string PluginGuid = "A1B9BEA0-EC3C-43E8-B660-08A993108EB5";
        private const string PluginHelp = "www.m0rkeulv.net";
        private const string PluginName = "ImportResolver";

        // settings object and path
        protected string SettingsPath;
        protected Settings settings;

        #region Required Properties

        public PluginMain()
        {
            settings = new Settings();
        }

        /// <summary>
        ///     Api level of the plugin
        /// </summary>
        public Int32 Api
        {
            get { return 1; }
        }

        /// <summary>
        ///     Name of the plugin
        /// </summary>
        public String Name
        {
            get { return PluginName; }
        }

        /// <summary>
        ///     GUID of the plugin
        /// </summary>
        public String Guid
        {
            get { return PluginGuid; }
        }

        /// <summary>
        ///     Author of the plugin
        /// </summary>
        public String Author
        {
            get { return PluginAuth; }
        }

        /// <summary>
        ///     Description of the plugin
        /// </summary>
        public String Description
        {
            get { return PluginDesc; }
        }

        /// <summary>
        ///     Web address for help
        /// </summary>
        public String Help
        {
            get { return PluginHelp; }
        }

        /// <summary>
        ///     Object that contains the settings
        /// </summary>
        [Browsable(false)]
        public Object Settings
        {
            get { return settings; }
        }

        #endregion

        #region initialize_methods

        /// <summary>
        ///     Initializes the plugin
        /// </summary>
        public void Initialize()
        {
            //find settings path
            String dataPath = Path.Combine(PathHelper.DataDir, SettingsFolder);
            if (!Directory.Exists(dataPath)) Directory.CreateDirectory(dataPath);
            SettingsPath = Path.Combine(dataPath, SettingsFile);

            // register shortcut with default values as params
            PluginBase.MainForm.RegisterShortcutItem(Shortcut, (Keys.Alt | Keys.Control | Keys.I));

            LoadSettings();
            AddEventHandlers();
        }

        /// <summary>
        ///     Disposes the plugin
        /// </summary>
        public void Dispose()
        {
            SaveSettings();
        }

        /// <summary>
        ///     Saves plugin settings.
        /// </summary>
        public void SaveSettings()
        {
            ObjectSerializer.Serialize(SettingsPath, settings);
        }

        /// <summary>
        ///     Loads plugin settings.
        /// </summary>
        public void LoadSettings()
        {
            Object obj = ObjectSerializer.Deserialize(SettingsPath, settings);
            settings = (Settings) obj;
        }

        /// <summary>
        ///     Adds the required event handlers
        /// </summary>
        public void AddEventHandlers()
        {
            // listen for keys so we can look for shortcuts
            EventManager.AddEventHandler(this, EventType.Keys);
        }

        #endregion

        #region plugin_logic

        /// <summary>
        ///     Handles all events that we listen for.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The event.</param>
        /// <param name="priority">The Handling priority.</param>
        public void HandleEvent(object sender, NotifyEvent e, HandlingPriority priority)
        {
            if (e.Type == EventType.Keys)
            {
                // convert event and extract key info
                Keys keys = (e as KeyEvent).Value;

                // if our shortcut was pessed, call AddMissingImports.
                if (keys == PluginBase.MainForm.GetShortcutItemKeys(Shortcut))
                {
                    AddMissingImports();
                }
            }
        }

        /// <summary>
        ///     Adds missing import statments for current active document
        ///     NOTE: will only add statments for types that are destinct.
        ///     ex.if 2 classes in diffrent packages has the same name this code will any code using either of the classes as it
        ///     cant determine witch one to use.
        /// </summary>
        public  void AddMissingImports()
        {
            ScintillaControl sci = ASContext.CurSciControl;
            sci.BeginUndoAction();
            // we make a local copy of the list of classes to avoid modification problem cause by us editing the file.
            var classes = new List<ClassModel>();
            classes.AddRange(ASContext.Context.CurrentModel.Classes);

            foreach (ClassModel classmodel in classes)
            {
                ASContext.Context.UpdateCurrentFile(false);
                // get current position so we can put the cursor back on the correct line
                int currentPos = sci.CurrentPos;
                int currentLine = sci.LineFromPosition(currentPos);
                int currentLinePos = currentPos - (sci.PositionFromLine(currentLine));

                // count number of lines  before chages so we can compare later
                int linesBefore = sci.LineCount;
                // check if our cursoer is  inside the class or below
                Boolean insideOrBelow = currentLine > classmodel.LineFrom + 1;

                // set pos inside class
                sci.CurrentPos = sci.PositionFromLine(classmodel.LineFrom + 1);
                sci.SetSel(sci.CurrentPos, sci.CurrentPos);

                // update context after moving cursor to class 
                ASContext.Context.UpdateCurrentFile(true);
                ASContext.Context.SetOutOfDate();

                // Find and add imports
                List<MemberModel> imports = FindPotentionalImports(sci, classmodel);
                AddImportStatementsInClass(imports, classmodel);

                // count lines after change and calculate diffrence
                int linesAfter = sci.LineCount;
                int difference = linesAfter - linesBefore;


                if (!insideOrBelow)
                {
                    // if we where above the changes just move cursor back
                    sci.CurrentPos = currentPos;
                }
                else
                {
                    // if we where below  change possition to  line with line numer  old +  those added and move cursor to correct possition on that line
                    sci.CurrentPos = sci.PositionFromLine(currentLine + difference) + currentLinePos;
                }
                sci.SetSel(sci.CurrentPos, sci.CurrentPos);
            }
            sci.EndUndoAction();
        }


        /// <summary>
        ///     Adds  import statements to document.
        /// </summary>
        /// <param name="imports">The list of imports we  want to add.</param>
        /// <param name="classmodel">The class witch we want to add imports to.</param>
        private static void AddImportStatementsInClass(List<MemberModel> imports, ClassModel classmodel)
        {
            string package = classmodel.InFile.FullPackage;

            foreach (MemberModel member in imports)
            {
                if (member.Type.Substring(0, member.Type.LastIndexOf('.')) != package)
                    if (ASContext.Context.CurrentModel.Imports.Items.Find(TypeContains(member.Name)) == null)
                    {
                        ASGenerator.InsertImport(member, true);
                        // update context so it wont  insert imports for types allready imported
                        ASContext.Context.UpdateCurrentFile(false);
                    }
            }
        }

        /// <summary>
        ///     Finds all potentional imports for a given class.
        /// </summary>
        /// <param name="sci">The ScintillaControl contiaing the code/ document.</param>
        /// <param name="classmodel">The class we want to scan.</param>
        /// <returns>List of potentional imports</returns>
        private static List<MemberModel> FindPotentionalImports(ScintillaControl sci, ClassModel classmodel)
        {
            MemberModel model = null;
            var importList = new List<MemberModel>();


            // check if we extends a class  (or interface for interfaces) and add to potentional imports list
            model = SearchForTypeInProject(classmodel.ExtendsType);
            if (isMemberAValidClassOrInterface(model))
            {
                importList.Add(model);
            }

            // check if class implements any interface and add those to potentional imports
            if (classmodel.Implements != null)
                foreach (string Interface in classmodel.Implements)
                {
                    model = SearchForTypeInProject(Interface);
                    if (isMemberAValidClassOrInterface(model))
                    {
                        importList.Add(model);
                    }
                }

            // check all class members for potentional imports ( member variables, method parameters and any local variable in methods)
            if (classmodel.Members != null)
                foreach (MemberModel member in classmodel.Members.Items)
                {
                    // if member is method scan  parameters 
                    if ((member.Flags & FlagType.Function) == FlagType.Function)
                    {
                        // extract types from parameter list and add to list (including return type)
                        if (member.Parameters != null)
                            foreach (MemberModel parameter in member.Parameters)
                            {
                                model = SearchForTypeInProject(parameter.Type);
                                if (isMemberAValidClassOrInterface(model))
                                {
                                    importList.Add(model);
                                }
                            }
                        // extract list of variables in methods and add types to list
                        MemberList localVars = ExtractLocalVariablesFromMethod(sci, member);
                        foreach (MemberModel localVar in localVars)
                        {
                            model = SearchForTypeInProject(localVar.Type);
                            if (isMemberAValidClassOrInterface(model))
                            {
                                importList.Add(model);
                            }
                        }
                    }
                        // member variables
                    else
                    {
                        model = SearchForTypeInProject(member.Type);
                        if (isMemberAValidClassOrInterface(model))
                        {
                            importList.Add(model);
                        }
                    }
                }


            return importList;
        }

        /// <summary>
        ///     Extracts a list of local variables from method.
        /// </summary>
        /// <param name="sci">The ScintillaControl with the active document.</param>
        /// <param name="member">The method we want to extract variables from.</param>
        /// <returns>List of variables decleard in method</returns>
        private static MemberList ExtractLocalVariablesFromMethod(ScintillaControl sci, MemberModel member)
        {
            var exp = new ASExpr();
            exp.ContextMember = member;
            exp.ContextFunction = member;
            int start = sci.PositionFromLine(member.LineFrom);
            int end = sci.LineEndPosition(member.LineTo);
            exp.FunctionBody = sci.Text.Substring(start, end - start);
            return ASComplete.ParseLocalVars(exp);
        }

        /// <summary>
        ///     Determines whether memberModel instance is a valid class or interface.
        /// </summary>
        /// <param name="model">The memberModel instance.</param>
        /// <returns>true if it is either a class or interface;Otherwise false.</returns>
        private static bool isMemberAValidClassOrInterface(MemberModel model)
        {
            return model != null &&
                   ((model.Flags & FlagType.Class) == FlagType.Class ||
                    (model.Flags & FlagType.Interface) == FlagType.Interface);
        }

        /// <summary>
        ///     Searches the entire project for a class/type with  provided name
        /// </summary>
        /// <param name="name">The name of the class/type we are looking for</param>
        /// <returns>MemberModel with class information if one and only one match was found</returns>
        private static MemberModel SearchForTypeInProject(String name)
        {
            List<MemberModel> results = ASContext.Context.GetAllProjectClasses().Items.FindAll(TypeContains("." + name));
            if (results.Count == 1) return results[0];
            return null;
        }

        /// <summary>
        ///     Predicate that check if a MemberModels name the contains the provided name "part".
        /// </summary>
        /// <param name="part">the last part of the full class name.</param>
        /// <returns></returns>
        private static Predicate<MemberModel> TypeContains(String part)
        {
            return delegate(MemberModel model)
            {
                if (part == null) return false;
                return model.Type.EndsWith(part);
            };
        }

        #endregion
    }
}