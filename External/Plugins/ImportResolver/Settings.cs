﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using System.Xml.Serialization;
using PluginCore;

namespace ImportResolver
{
    [Serializable]
    public class Settings
    {
        private const string PluginVersion = "1.0";
        

        private bool formatCode = true;
        private bool organizeImports = true;
        private bool resolveImports = true;

        [Category("1. About")]
        [DisplayName("Plugin Version")]
        public string Version
        {
            get { return PluginVersion; }
        }

        [Category("2. Shortcuts")]
        [DisplayName("Add Missing imports")]
        [Description("To change shortcuts go to Tools->Keyboard shortcuts")]
        [XmlIgnore]
        public Keys AddIMportsShortCut
        {
            get { return PluginBase.MainForm.GetShortcutItemKeys(PluginMain.Shortcut); }
        }
    }
}